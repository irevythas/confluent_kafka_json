from confluent_kafka import Producer

p = Producer({'bootstrap.servers': 'localhost:9092'})
some_data_source=['{"a1":1, "a2":2}']
for data in some_data_source:
    p.produce('confluent-kafka-topic', data.encode('utf-8'))
p.flush()
