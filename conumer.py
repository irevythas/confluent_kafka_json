from confluent_kafka import Consumer, KafkaError
import ast

c = Consumer({'bootstrap.servers': 'localhost:9092', 'group.id': 'my-group',
              'default.topic.config': {'auto.offset.reset': 'smallest'}})
c.subscribe(['confluent-kafka-topic'])
running = True
while running:
    msg = c.poll()
    if not msg.error():
        val = ast.literal_eval(msg.value().decode('utf-8'))
        print('Received message: ')
        print(val)
        print(type(val))
    elif msg.error().code() != KafkaError._PARTITION_EOF:
        print(msg.error())
        running = False
c.close()
